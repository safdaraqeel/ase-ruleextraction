package CPLRules;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ReadRules {

	public static CPLRule[] getRulesFromFile(String filePath) {
		String text = "";
		File fileName = new File(filePath);
		try {
			Scanner scan = new Scanner(fileName);
			boolean startFlag=false;
			boolean endFlag=true;
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				if (line.contains("---------------")){	
					line = scan.nextLine();
					startFlag=true;
					endFlag=false;
				}
				if (line.contains("Number of Leaves")||line.contains("Number of Rules")||line.contains("): FailedFailed ")||line.contains("): ConnectedConnected")||line.contains("): ConnectedFailed")||line.contains("): FailedConnected")){	
					line = scan.nextLine();
					endFlag=true;
				}
								
				if (startFlag && !endFlag){
				text += line.replaceAll("AND", " AND ").replaceAll("  ", " ")
						.replace(System.getProperty("line.separator"), "");
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		// text.replaceAll("AND", " AND ");
		System.out.println(text);
		String[] temp = text.split("\\)");
		CPLRule[] rules = new CPLRule[temp.length];
		/* print substrings */
		for (int i = 0; i < temp.length; i++) {
			rules[i] = new CPLRule();
			String[] temp2 = temp[i].split("\\(");
			if (temp2[0] != null) {
				rules[i].setExpression(temp2[0]);
				if (rules[i].getExpression().contains("ConnectedConnected")) {
					rules[i].setSystemState("ConnectedConnected");
				} else if (rules[i].getExpression().contains("FailedFailed")) {
					rules[i].setSystemState("FailedFailed");
				} else if (rules[i].getExpression().contains("ConnectedFailed")) {
					rules[i].setSystemState("ConnectedFailed");
				} else if (rules[i].getExpression().contains("FailedConnected")) {
					rules[i].setSystemState("FailedConnected");
				} else {
					rules[i].setSystemState("Invalid");
				}
				String [] temp4= rules[i].getExpression().split(":");
				rules[i].setExpression(temp4[0]);
			}
			if (temp2[1] != null && temp2[1].contains("/") == false) {
				rules[i].setSupport(Double.parseDouble(temp2[1]));
				rules[i].setViolate(0);
				if (rules[i].getSupport()+rules[i].getViolate()==0){
					rules[i].setConfidence(0);;
				}
				else {
					rules[i].setConfidence(1);
				}
			}

			if (temp2[1] != null && temp2[1].contains("/") == true) {
				String[] temp3 = temp2[1].split("/");
				rules[i].setSupport(Double.parseDouble(temp3[0]));
				rules[i].setViolate(Double.parseDouble(temp3[1]));
				double conf= (rules[i].getSupport()-rules[i].getViolate())/(rules[i].getSupport()+rules[i].getViolate());
				rules[i].setConfidence(conf);
			}
		}
		return rules;
	}
}
