package CPLRules;

public class CPLRule {
    private String expression;
    private boolean result;
    private double support;
    private double violate;
    private String systemState;
    private double confidence;

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getSystemState() {
        return systemState;
    }

    public void setSystemState(String systemState) {
        this.systemState = systemState;
    }

    public boolean getResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public double getSupport() {
        return support;
    }

    public void setSupport(double support) {
        this.support = support;
    }

    public double getViolate() {
        return violate;
    }

    public void setViolate(double violate) {
        this.violate = violate;
    }


    public void printRule() {
        System.err.println("Exp: " + this.getExpression() + ", SystemState: " + this.getSystemState() + " {Support: " + this.getSupport()
                + ", Violation: " + this.getViolate() + ", TotalDataUsed: " + (this.getSupport() + this.getViolate()) + ", confidence: " + (this.getSupport() - this.getViolate()) / (this.getSupport() + this.getViolate()) + "}");
    }

    public void printConstraintResult() {
        System.err.println("Exp: " + this.getExpression() + " Result: " + this.getResult());

    }
}
