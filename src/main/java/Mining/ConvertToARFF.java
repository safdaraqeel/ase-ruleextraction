package Mining;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ConvertToARFF {

	public static void converCSV2ARFF(String sourceFilePath,String destinationFilePath){
		
		String header ="@relation VCS_DataSet\n\n\n"+
				"@attribute P1_Protocol {SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}\n"+
				"@attribute P2_Protocol {SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}\n"+
				"@attribute P3_Protocol {SIP,AIM,GoogleTalk,ICQ,iPPi,iptel.org,IRC}\n"+

				"@attribute P1_SIP_Listen_Port {On, Off}\n"+
				"@attribute P2_SIP_Listen_Port {On, Off}\n"+
				"@attribute P3_SIP_Listen_Port {On, Off}\n"+

				"@attribute P1_Transport_Protocol {Auto, UDP,TCP,TLS}\n"+
				"@attribute P2_Transport_Protocol {Auto, UDP,TCP,TLS}\n"+
				"@attribute P3_Transport_Protocol {Auto, UDP,TCP,TLS}\n"+

				"@attribute P1_Encryption {On, Off, BestEffort}\n"+
				"@attribute P2_Encryption_s2 {On, Off, BestEffort}\n"+
				"@attribute P3_Encryption_s3 {On, Off, BestEffort}\n"+

				"@attribute P1_SIP_Zrtp_Attribute {Auto, TRUE, FALSE}\n"+
				"@attribute P2_SIP_Zrtp_Attribute {Auto, TRUE, FALSE}\n"+
				"@attribute P3_SIP_Zrtp_Attribute {Auto, TRUE, FALSE}\n"+

				"@attribute P1_Max_Peers_Allowed_In_Conference_Call {2,3,4,5}\n"+
				"@attribute P2_Max_Peers_Allowed_In_Conference_Call {2,3,4,5}\n"+
				"@attribute P3_Max_Peers_Allowed_In_Conference_Call {2,3,4,5}\n"+

				"@attribute P1_MTU numeric\n"+
				"@attribute P2_MTU numeric\n"+
				"@attribute P3_MTU numeric\n"+

				"@attribute P1_Default_Callrate numeric\n"+
				"@attribute P2_Default_Callrate numeric\n"+
				"@attribute P3_Default_CallRate numeric\n"+

				"@attribute P1_Max_Receive_Callrate numeric\n"+
				"@attribute P2_Max_Receive_Callrate numeric\n"+
				"@attribute P3_Max_Receive_Callrate numeric\n"+

				"@attribute P1_Max_Transmit_Callrate numeric\n"+
				"@attribute P2_Max_Transmit_Callrate numeric\n"+
				"@attribute P3_Max_Transmit_Callrate numeric\n"+

				"@attribute P1_Audio_Codec  {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}\n"+
				"@attribute P2_Audio_Codec  {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}\n"+
				"@attribute P3_Audio_Codec  {Auto,opus-48000,SILK-24000,SILK-16000,G722-16000,speex-32000,speex-16000,PCMU-8000,PCMA-8000,iLBC-8000,GSM-8000,speex-8000,AMR-WB-16000,SILK-12000,SILK-8000,telephone-event-80000}\n"+

				"@attribute P1_Video_Codec {Auto, h264,red,rtx,ulpfec,VP8}\n"+
				"@attribute P2_Video_Codec {Auto, h264,red,rtx,ulpfec,VP8}\n"+
				"@attribute P3_Video_Codec  {Auto, h264,red,rtx,ulpfec,VP8}\n"+

				"@attribute P1_Max_Resolution {1080,720,480,360,240}\n"+
				"@attribute P2_Max_Resolution {1080,720,480,360,240}\n"+
				"@attribute P3_Max_Resolution {1080,720,480,360,240}\n"+

				"@attribute System_State {FailedFailed,ConnectedConnected,FailedConnected,ConnectedFailed}\n\n"+
				"@data\n\n\n";
				
		String data="";

		File sourceFile = new File(sourceFilePath);
		File destinationFile = new File(destinationFilePath);
		try {
			Scanner scan = new Scanner(sourceFile);
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				line=line.replaceAll(",", "").replaceAll("@", "").replaceAll(":", "").replaceAll("E+", "E");
				if (!line.toLowerCase().contains("encryption")){
				line=line.replaceAll(";", ",")+"\n";
				data+=line;
				}
			}
			
	    	// writing again
	    	FileWriter fw = new FileWriter(destinationFile,false);
	    	BufferedWriter bw = new BufferedWriter(fw);
	    	bw.write(header+data);
	    	bw.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
}
	
	
	public static void main(String[] args) {
		converCSV2ARFF("src/resources/Datasets/ConfigurationsAndStatuses.csv","AllData.arff");
	}
	
	
	
	

}
