package Main_App_Runner;

import CPLRules.WriteRulesIntoExelFile;
import Mining.C45Application;
import CPLRules.CPLRule;
import CPLRules.ReadRules;


public class Mining_App {

    public static void main( String[] args ) throws Exception {
        String baseFolderPath = "src/main/resources/Datasets/";
        String configurationDataPath = baseFolderPath + "ConfigurationsAndStatuses.csv";

        //Output Files
        String c45ConstraintsPath = baseFolderPath+"C45_Constraints.txt";
        String treeDestinationFilePath = baseFolderPath+"C45_Tree.txt";

        //mining rules with C4.5
        C45Application.run(configurationDataPath,treeDestinationFilePath, c45ConstraintsPath, 0);
        Thread.sleep(5000);

        // writing the rules into excel file and clustering
        CPLRule[] c45_Constraints= ReadRules.getRulesFromFile(c45ConstraintsPath);
        WriteRulesIntoExelFile.WriteRulesToExcel(baseFolderPath+"Final_RuleSet.xlsx", c45_Constraints);

        System.err.println("-------------------The extracted rules are as follow-------------------");
        for (int i = 0; i < c45_Constraints.length; i++) {
            c45_Constraints[i].printRule();
        }
    }

}



