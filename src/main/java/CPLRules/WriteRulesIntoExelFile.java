package CPLRules;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;

public class WriteRulesIntoExelFile {

	
	public static void WriteRulesToExcel(String filepath, CPLRule[] rules) {
		Sheet sheet = null;
		try {
			InputStream inp;
			Workbook wb;

			int sheetnumber = 0;
			File file = new File(filepath);
			
			if (file.exists()){
				inp = new FileInputStream(filepath);
				wb = WorkbookFactory.create(inp);
				wb.createSheet("CPL Rules");
				sheet = wb.getSheetAt(wb.getNumberOfSheets()-1);
				
			} else{
				wb= new XSSFWorkbook();
				wb.createSheet("CPL Rules");
				sheet = wb.getSheetAt(wb.getNumberOfSheets()-1);
			}

			Row row = sheet.createRow(0);
			Cell cell1 = row.createCell(0);
			cell1.setCellType(1);
			cell1.setCellValue("Constraint Expression");

			cell1 = row.createCell(1);
			cell1.setCellType(1);
			cell1.setCellValue("System State");
			
			cell1 = row.createCell(2);
			cell1.setCellType(1);
			cell1.setCellValue("Violation");

			cell1 = row.createCell(3);
			cell1.setCellType(1);
			cell1.setCellValue("Support");
			
			cell1 = row.createCell(4);
			cell1.setCellType(1);
			cell1.setCellValue("Confidence");
			
			for (int i=1; i<=rules.length;i++){
				row = sheet.createRow(i);
				
				Cell cell2 = row.createCell(0);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getExpression());

				cell2 = row.createCell(1);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getSystemState());
				
				cell2 = row.createCell(2);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getViolate());

				cell2 = row.createCell(3);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getSupport());
				
				cell2 = row.createCell(4);
				cell2.setCellType(1);
				cell2.setCellValue(rules[i-1].getConfidence());
			}
				
			FileOutputStream fileOut = new FileOutputStream(filepath);
			wb.write(fileOut);
			fileOut.close();
			wb.close();

		} catch (IOException|EncryptedDocumentException|InvalidFormatException e) {
			e.printStackTrace();
		}
	}
	

public static void main(String []args){
	String baseFolderPath="src/main/resources/Datasets/";
	String constraintsOutputFilePath=baseFolderPath+"C45_Constraints.txt";
	CPLRule[] constraints= ReadRules.getRulesFromFile(constraintsOutputFilePath);
	WriteRulesIntoExelFile.WriteRulesToExcel(baseFolderPath+"Final_RuleSet.xlsx", constraints) ;
}
	
		
	}
