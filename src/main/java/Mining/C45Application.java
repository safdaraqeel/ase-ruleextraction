package Mining;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;

public class C45Application {

	public static void mineRules(String sourceFilePath,String destinationFilePath, int numberAttributesToBeRemoved){
		try {
			ConvertToARFF.converCSV2ARFF(sourceFilePath,"Data.arff");
			DataSource source = new DataSource("Data.arff");
		 	Instances data = source.getDataSet();
			System.out.println("Total Attributes (including class) are:"+data.numAttributes());

			 // saving after removing certain attributes
			 ArffSaver saver = new ArffSaver();
             saver.setInstances(data);
             saver.setFile(new File("AllData.arff"));
             saver.writeBatch();

			 // setting class attribute if the data format does not provide this information
			 // For example, the XRFF format saves the class attribute information as well
			 if (data.classIndex() == -1)
			   data.setClassIndex(data.numAttributes() - 1);
	        	System.out.println(data.toSummaryString());

	        	// writing the rules to outputfile
	        	System.setOut(new PrintStream(new FileOutputStream(destinationFilePath)));
	           
	        	//Make tree
	            J48 tree = new J48();
	            String[] argv = {"-t","AllData.arff","-i","-x","10","-c","last"};
	            tree.setOptions(argv);
	            tree.buildClassifier(data);
	            tree.main(argv);
	            System.out.println(tree.graph().toString());
	            System.setOut(new PrintStream(System.out));
	            
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	 
	 public static void main(String[] args) throws Exception {
		mineRules("AllData.csv","AllData.arff", 0);
		String tree = new String(Files.readAllBytes(Paths.get("tree.txt")), StandardCharsets.UTF_8);
		 String [] array=tree.split("\n");
		 List<String> textualTreeNodes = Arrays.asList(array);
		 ExtractRuleFromDescisionTree.extractRules("tree.txt","J48Rules.txt");
	}
	 
	 public static void run(String sourceFilePath,String destinationFilePath,String rulesDestinationFilePath, int numberAttributesToBeRemoved) throws Exception {
			mineRules(sourceFilePath,destinationFilePath, numberAttributesToBeRemoved);
			String tree = new String(Files.readAllBytes(Paths.get(destinationFilePath)), StandardCharsets.UTF_8);
			 //System.out.println(tree);
//			 String [] array=tree.split("\n");
//			 List<String> textualTreeNodes = Arrays.asList(array);
			 ExtractRuleFromDescisionTree.extractRules(destinationFilePath,rulesDestinationFilePath);
		}
	
}
